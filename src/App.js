import React, { useState } from "react";
import Post from "./components/Post"

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      posts: [1, 2, 3, 4, 5],
      inputValue: '0'
    }
  }


  whenClick = () => {
    this.setState({
      posts: [ ...this.state.posts, this.state.inputValue]
    })
  }

  changeInputValue = (event) => {
    this.setState({inputValue: event.target.value})
  }

  render() {
    return (

          <div className="App">
          <div>
            <input value={this.state.inputValue} onChange={this.changeInputValue}></input>
            <button onClick={this.whenClick}>Click Me!</button>
          </div>
    
          <ul>
            {this.state.posts.map((post) => {
              return <li>{post}</li>
            })}
          </ul>
    
        </div>


    /*        <div className="App">
          <h1>{likes}</h1>
          <h1>{value}</h1>
          <input 
            type="text"
            value={value}
            onChange={event => setValue(event.target.value)}
            />
          <button onClick={increment}>Increment</button>
          <button onClick={decrement}>Decrement</button>
          <Post></Post>
          <Post></Post>*/
      )
  }

}

/*function App() {
  const posts = [1, 2, 3, 4, 5, 6]

  const [likes, setLikes] = useState(0)
  const [value, setValue] = useState('Текст')

  function increment() {
    setLikes(likes + 1)
  }

  function decrement() {
    setLikes(likes - 1)
  }
}*/

export default App;
